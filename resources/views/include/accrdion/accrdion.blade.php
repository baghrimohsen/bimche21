
</div>
<div class="row mt-5 ml-4" style="margin-bottom: 150px">


    <div class=" col-7 text-center">

<!-- Card -->
<div class=""
style=" ">

<!-- Content -->
<div class=" px-4">
  <div class="row d-flex justify-content-center">
    <div class="col-md-10 col-xl-8">

      <!--Accordion wrapper-->
      <div class="accordion md-accordion accordion-5" id="accordionEx5" role="tablist"
        aria-multiselectable="true">

        <!-- Accordion card -->
        <div class="card mb-4">

          <!-- Card header -->
          <div class="card-header p-0 z-depth-1 text-right" role="tab" id="heading30">
            <a data-toggle="collapse" data-parent="#accordionEx5" href="#collapse30" aria-expanded="true"
              aria-controls="collapse30">
              <i class=" fa-2x p-1 mr-4 float-left black-text" aria-hidden="true"><img src="https://img.icons8.com/plasticine/50/000000/shopping-basket.png"/></i>
              <h6 class="text-info text-center mb-0 py-3 mt-1 ">
                بررسی انواع سبدهای بیمه 
              </h6>
            </a>
          </div>

          <!-- Card body -->
          <div id="collapse30" class="collapse show" role="tabpanel" aria-labelledby="heading30"
            data-parent="#accordionEx5">
            <div class="card-body rgba-black-light white-text z-depth-1">
              <p class="p-md-4 mb-0">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
            </div>
          </div>
        </div>
        <!-- Accordion card -->

        <!-- Accordion card -->
        <div class="card mb-4">

          <!-- Card header -->
          <div class="card-header p-0 z-depth-1" role="tab" id="heading31">
            <a data-toggle="collapse" data-parent="#accordionEx5" href="#collapse31" aria-expanded="true"
              aria-controls="collapse31">
              <i class=" p-1 mr-4 float-left black-text" aria-hidden="true"><img src="https://img.icons8.com/plasticine/50/000000/company.png"/></i>
              <h6 class="text-uppercase text-info mb-0 py-3  mt-1">
                مقایسه خدمات شرکت های مختلف 
              </h6>
            </a>
          </div>

          <!-- Card body -->
          <div id="collapse31" class="collapse" role="tabpanel" aria-labelledby="heading31"
            data-parent="#accordionEx5">
            <div class="card-body rgba-black-light white-text z-depth-1">
              <p class="p-md-4 mb-0">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
            </div>
          </div>
        </div>
        <!-- Accordion card -->

        <!-- Accordion card -->
        <div class="card mb-4">

          <!-- Card header -->
          <div class="card-header p-0 z-depth-1" role="tab" id="heading32">
            <a data-toggle="collapse" data-parent="#accordionEx5" href="#collapse32" aria-expanded="true"
              aria-controls="collapse32">
              <i class="fas fa-cogs fa-2x p-3 mr-4 float-left black-text" aria-hidden="true"></i>
              <h6 class="text-uppercase text-info mb-0 py-3 mt-1">
                خرید آنلاین و تحویل درب منزل 
              </h6>
            </a>
          </div>

          <!-- Card body -->
          <div id="collapse32" class="collapse" role="tabpanel" aria-labelledby="heading32"
            data-parent="#accordionEx5">
            <div class="card-body rgba-black-light white-text z-depth-1">
              <p class="p-md-4 mb-0">لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد</p>
            </div>
          </div>
        </div>
        <div class="card mb-4">

            <!-- Card header -->
            <div class="card-header p-0 z-depth-1" role="tab" id="heading33">
              <a data-toggle="collapse" data-parent="#accordionEx5" href="#collapse33" aria-expanded="true"
                aria-controls="collapse32">
                <i class="fas fa-cogs fa-2x p-3 mr-4 float-left black-text" aria-hidden="true"></i>
                <h6 class="text-uppercase text-info mb-0 py-3 mt-1">
                    مشاوره و پشتیبانی مستمر 
                </h6>
              </a>
            </div>
  
            <!-- Card body -->
            <div id="collapse33" class="collapse" role="tabpanel" aria-labelledby="heading33"
              data-parent="#accordionEx5">
              <div class="card-body rgba-black-light white-text z-depth-1">
                <p class="p-md-4 mb-0">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
                  terry richardson ad squid. 3 wolf moon officia aute,
                  non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch
                  3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda
                  shoreditch et.</p>
              </div>
            </div>
          </div>
        <!-- Accordion card -->
      
      </div>
      <!--/.Accordion wrapper-->

    </div>
  </div>
</div>
<!-- Content -->
</div>
<!-- Card -->
    </div>
    
    <div class=" col-5 text-center p-5">
        <p class="" style="font-size: 2rem;color: cornflowerblue">رسالت اجتماعی</p>
        <span>
    
        شما با خرید هر یک بیمه نامه از بیمچه، در تامین بخشی از هزینه های درمان یک کودک سرطانی، کمک به کودکان کار، تامین مایحتاج افراد سیل و زلزله زده و… شریک هستید…
        
        چون این اصلی ترین رسالت بیمچست!
        </span></div>
</div>

