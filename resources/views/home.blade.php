@extends('layouts.app')

@section('content')
@include('include.navbar.navbar')

@if($back_home !== 1)
@include('include.sidbar.sidbar')
@endif


@if($icon === "fire")
     @include('form.fire')
@endif
@if($icon === 0)
        @include('icon-select.icon')
@endif
@include('include.jumbotron.jumbotron')
@include('include.accrdion.accrdion')
@include('include.footer.footer')
@endsection
