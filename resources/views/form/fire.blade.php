

<div class="mt-5 container">
<div class="text-center mb-5">
    <h4>سامانه مقایسه و خرید آنلاین بیمه</h4>
</div>

<div class="d-flex flex-column card ">
    <div class="text-center bg-info rounded-top p-4 text-white">
        <span class="text-right d-flex "><i class="m-1 fas fa-1x fa-angle-right"></i><span><a href="/back_home" class="text-white" >همه بیمه ها </a>  </span></span>
        <span>بیمه اتش سوزی</span>
  
    </div>
    <div class="alert alert-danger mt-3 mx-2 text-center d-none" id="alert"></div>
    <div class="p-4 text-center " id="building">
        <span>نوع ملک خود را انتخاب کنید و در صورت نیاز تعداد واحدهای آن را وارد کنید.</span>

        <div class="d-flex justify-content-around mt-5">

            
   
            <button class="btn bg-light" >قبلی</button>  
<select class="browser-default custom-select mt-2" style="width: 30%" id="buildingـtype_select">
    <option selected value="null" >  نوع ملک </option>
    <option value="1">یک واحد در اپارتمان</option>
    <option value="2">یک ساختمان ویلایی</option>
    <option value="3">اپارتمان یا مجتمع</option>
  </select>
<input type="text" class="form-control mt-2" placeholder="تعداد واحد" style="width: 30%" id="order_unit">
                       
               
             
                <button class="btn bg-info" onclick="buildingـtype()" >بعدی</button>
                
        </div>
    </div>

    {{--  مرحله دوم  --}}
    <div class="p-4 text-center d-none" id="structure_type" >
  
<span>نوع سازه‌ی ملک خود را انتخاب کنید.</span>
        <div class="d-flex justify-content-around mt-5">

            
   
            <button class="btn bg-info" onclick="structure_type()" >قبلی</button>  
<select class="browser-default custom-select mt-2" style="width: 60%" id="structure_select" >
    <option selected value="null">   نوع سازه </option>
    <option value="1">اجری</option>
    <option value="2">فلزی</option>
    <option value="3">  بتنی</option>
  </select>
      
             
                <button class="btn bg-info" onclick="structure_type_after()" >بعدی</button>
                
        </div>
    </div>

    {{--  تمام۲  --}}
    {{--  3  --}}
    <div class="p-4 text-center d-none" id="old_struct">
  
        <span>عمر ملک خود را انتخاب کنید  </span>
                <div class="d-flex justify-content-around mt-5">
        
                    
           
                    <button class="btn bg-info" onclick="old_struct()" >قبلی</button>  
        <select class="browser-default custom-select mt-2" style="width: 60%" id="old_struct_select">
           <option selected value="null">عمر بنا</option>
            <option value="1">1 سال</option>
            <option value="2">۲ سال</option>
            <option value="3">  ۳ سال</option>
          </select>
              
                     
                        <button class="btn bg-info" onclick="old_struct_after()" >بعدی</button>
                        
                </div>
            </div>
        

    {{--  end 3  --}}

    {{--  4 --}}
    <div class="p-4 text-center d-none" id="metras">
  
        <span> متراژ ملک مورد نظر را وارد کنید<span>
                <div class="d-flex justify-content-around mt-5">
        
                    
           
                    <button class="btn bg-info" onclick="metras()" >قبلی</button>  
  
                    <input type="text" class="form-control mt-2" placeholder="متراژ مورد بیمه(متر مربع)" id="metras_val">
              
                     
                        <button class="btn bg-info" onclick="metras_after()" >بعدی</button>
                        
                </div>
            </div>
{{--  end 4  --}}

   {{--  5 --}}
   <div class="p-4 text-center  d-none" id="cost">
  
    <span> ارزش لوازم منزل خود را به استثنای پول نقد و طلا و جواهرات به تومان وارد کنید<span>
            <div class="d-flex justify-content-around mt-5">
    
                
       
                <button class="btn bg-info" onclick="cost()" >قبلی</button>  

                <input type="number" class="form-control mt-2" placeholder="ارزش لوازم منزل (تومان)">
          
                 
                    <button class="btn bg-info text-nowrap" >استعلام قیمت</button>
                    
            </div>
        </div>
{{--  end 5  --}}

</div>


</div>