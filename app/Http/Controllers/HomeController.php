<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 
        $icon = 0;
        $back_home = 0;
        return view('home',compact('icon','back_home'));
    }
    public function icon_select($id){

      $icon = 0;
      $back_home = 1;
     if($id == "fire"){
        $icon = "fire";
     }
     return view("home",compact("icon",'back_home'));   
    }

    public function back_home()
    { 
        $icon = 0;
        $back_home = 1;
        return view('home',compact('icon','back_home'));
    }
}
